<?php
/*
 * File : woo-wishlist-functions.php
 * Date : 21/11/2016
 * Author : Alex P
 *
 * Woo Wishlist general helper functions
 * 
 */

/*
 * Pretty print arrays
 *
 * @param $Array    Array to print
 */
function wwl_print_r( $Array )
{
    echo "<pre>";
    print_r($Array);
    echo "</pre>";
}