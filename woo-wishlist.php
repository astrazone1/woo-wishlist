<?php
/**
 * Plugin Name: WooCommerce Wishlist
 * Plugin URI: https://astrazone.com/
 * Description: WooCommerce plugin that allows users to add procucts to their wishlist.
 * Version: 1.0.0
 * Author: Alex P
 * Author URI: https://astrazone.com/
 *
 *
 * @package woo-wishlist
 * @category Core
 * @author AlexP
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit; 
}

// Include necessary files
include_once( 'woo-wishlist-config.php' );
include_once( 'woo-wishlist-functions.php' );
include_once( 'includes/class-woo-wishlist.php' );

// Wordpress hooks
add_action( 'init', array( 'WooWishList', 'init' ));
register_activation_hook( __FILE__, array( 'WooWishList', 'install_plugin' ));
register_uninstall_hook( __FILE__, array( 'WooWishList', 'uninstall_plugin' ));

// Wordpress Frontend (Add script and css)
add_action( 'wp_enqueue_scripts', array( 'WooWishList', 'setup_frontend' ) );

// Setup ajax hooks
add_action( 'wp_ajax_woo_wishlist_request', array( 'WooWishList', 'handle_wishlist_request' ) );
add_action( 'wp_ajax_nopriv_woo_wishlist_request', array( 'WooWishList', 'handle_wishlist_request' ) );

// WooCommerce hooks
add_action( 'woocommerce_after_add_to_cart_button', array( 'WooWishList', 'create_add_to_wishlist_button' ));
add_action( 'woocommerce_after_shop_loop_item', array( 'WooWishList', 'create_add_to_wishlist_button' ));

// Wordpress shortcode
add_shortcode( 'woo_wishlist_display', array( 'WooWishList', 'display_wishlist' ));

// Localization
add_action('plugins_loaded', array( 'WooWishList', 'localization' ));

?>