<?php
/*
 * File : includes/class-woo-handler-interface.php
 * Date : 21/11/2016
 * Author : Alex P
 *
 * Woo Wish List Handler Interface
 * Interface for handling the wishlist, could be a DB, cookie or anything else.
 * 
 */

// Exit if accessed directly.
if ( ! defined( 'WOO_WISH_LIST_DIRECT' ) ) {
	exit; 
}

/**
 * Woo Wish List Handler Interface
 *
 * @class iWooWishListHandler
 * @version	1.0.0
 */
interface iWooWishListHandler {
    /*
     * Install plugin.
     */
    public function install();
    
    /*
     * Uninstall plugin.
     */
    public function uninstall();

    /*
     *  Add Product to wishlist
     *
     * @param $Product_ID      Product ID to store
     */
    public function add_product_to_wishlist( $Product_ID );
    
    /*
     *  Remove Product from wishlist.
     */
    public function remove_product_from_wishlist( $WishList_ID );
    
    /*
     * Is The Product in Wish List
     */
    public function is_in_wishlist( $Product_ID );
    
    /*
     *  Retrive full wish list as array.
     */
    public function get_full_wish_list();
    
    /*
     *  Retrive Wish List By Product ID.
     */
    public function get_single_wish_list( $Product_ID );
    
}