<?php
/*
 * File : includes/class-woo-handler-db.php
 * Date : 21/11/2016
 * Author : Alex P
 *
 * Woo Wish List Database Handler
 * This handler stores the wishlist in the database and provides functions to 
 * store and retrive the values
 * 
 */

// Exit if accessed directly.
if ( ! defined( 'WOO_WISH_LIST_DIRECT' ) ) {
	exit; 
}

/**
 * Woo Wish List Database Handler
 *
 * @class WooWishListDBHandler
 * @version	1.0.0
 */
final class WooWishListDBHandler implements iWooWishListHandler{
    
    /*
     * Install plugin by creating a DB table.
     */
    public function install() {
        global $wpdb;
        $db_table_name = $wpdb->prefix . WOO_WISH_LIST_DB_TABLE_NAME;
        
        $table_query = "CREATE TABLE `{$db_table_name}` (
          `ID` int(11) NOT NULL AUTO_INCREMENT,
          `product_id` int(11) NOT NULL,
          `user_id` int(11) NOT NULL,
          `date_added` datetime NOT NULL,
          PRIMARY KEY (`ID`),
          KEY `user_id` (`user_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        // Use dbDelta to create the table (smarter creation)
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        
        $query_result = dbDelta( $table_query );
        
        // Table name in array is with quotes
        $created_table_name = '`' . $db_table_name . '`';

        // Validate that table was created
        if ( ! empty( $query_result[$created_table_name] && strpos( $query_result[$created_table_name], "Created table" ) !== false))
        {
            return true;
        }
        
        return false;
    }
    
    /*
     * Uninstall plugin by deleting the DB table and all records.
     */
    public function uninstall() {
        global $wpdb;
        $db_table_name = $wpdb->prefix . WOO_WISH_LIST_DB_TABLE_NAME;
        
        $wpdb->query( "DROP TABLE IF EXISTS " . $db_table_name );
    }


    /*
     *  Add Product to wishlist
     *
     *  @param $Product_ID      Product ID to store in the database.
     *
     *  @return mixed           Last inserted ID or FALSE if operation failed. 
     */
    public function add_product_to_wishlist( $Product_ID ) {
        global $wpdb;
        $db_table_name = $wpdb->prefix . WOO_WISH_LIST_DB_TABLE_NAME;
        $user = wp_get_current_user();
        
        // Validate that user isn't 0
        if ( ! empty( $user->ID ) && is_int( $Product_ID )) {
            $user_id = $user->ID;

            // Check that this wish doesn't already exist
            if ( $this->is_in_wishlist( $Product_ID ) === false ) {
                
                // Insert record
                $insert_result = $wpdb->insert( $db_table_name,
                                               array( 
                                                    'ID' => null,  // Auto incement
                                                    'product_id' => $Product_ID,
                                                    'user_id' => $user_id,
                                                    'date_added' => date("Y-m-d H:i:s") // eg. 2016-10-03 10:30:02
                                                ), 
                                                array( 
                                                    null,
                                                    '%d', 
                                                    '%d',
                                                    '%s'
                                                ) 
                                              );
                return $wpdb->insert_id;
            }
        }
        
        return false;
    }
    
    /*
     *  Is the product in the wishlist
     *
     *  @param $Product_ID      Product ID to check
     *
     *  @return mixed           ID of wishlist record
     *                          FALSE no record found or DB error
     */
    public function is_in_wishlist( $Product_ID ) {
        global $wpdb;
        $db_table_name = $wpdb->prefix . WOO_WISH_LIST_DB_TABLE_NAME;
        
        $user = wp_get_current_user();
        
        // Validate that user isn't 0
        if ( ! empty( $user->ID ) && is_int( $Product_ID ) )
        {
            $user_id = $user->ID;
            
            // Check that this wish doesn't already exist
            $check_results = $wpdb->get_row(
                                $wpdb->prepare( "SELECT `ID` FROM `{$db_table_name}` WHERE `product_id` = %d AND `user_id` = %d",
                                    array(
                                        $Product_ID,
                                        $user_id ))
                            );
            
            // If not empty mean that record exists
            if ( ! empty( $check_results ) ) {
                return intval($check_results->ID);
            }
        }
        
        return false;
    }
    
    /*
     *  Remove Product from wishlist.
     *
     *  @param $Wishlist_ID     ID to remove
     *
     *  @return boolean         TRUE if removed
     *                          FALSE if removal failed
     */
    public function remove_product_from_wishlist( $Wishlist_ID ) {
        global $wpdb;
        $db_table_name = $wpdb->prefix . WOO_WISH_LIST_DB_TABLE_NAME;
        
        if ( ! empty( $Wishlist_ID ) && is_int( $Wishlist_ID )) {
            
            // Return boolean value for removal outcome
            $delete_result = $wpdb->delete( $db_table_name, array( 'ID' => $Wishlist_ID ), array( '%d' ));
            
            return ( $delete_result !== false );
        }
        
        return false;
    }
    
    /*
     *  Retrive Wish List from DB for current user.
     *
     *  @return Array       Objects of wishlist_ID's and product_ID's
     */
    public function get_full_wish_list() {
        global $wpdb;
        
        $db_table_name = $wpdb->prefix . WOO_WISH_LIST_DB_TABLE_NAME;
        $wish_results = array();
        $user = wp_get_current_user();
        
        // Validate that user isn't 0
        if ( ! empty( $user->ID ))
        {
            $user_id = $user->ID;
            
            // Check that this wish doesn't already exist
            $wish_results = $wpdb->get_results(
                                $wpdb->prepare( "SELECT `ID`, `product_ID` FROM `{$db_table_name}` WHERE `user_id` = %d",
                                    array( $user_id )
                                ),
                                "ARRAY_A"
                            );
        }
        
        return $wish_results;
    }
    
    /*
     *  Retrive Wish List By Product ID.
     *
     *  @param $Product_ID      Product id to retrive the wishlist ID
     */
    public function get_single_wish_list( $Product_ID ) {
        return $this->is_in_wishlist( $Product_ID );
    }
    
}