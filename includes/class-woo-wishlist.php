<?php
/*
 * File : includes/class-woo-wishlist.php
 * Date : 21/11/2016
 * Author : Alex P
 *
 * Core Woo Wishlist Class
 * Contains core initialization process and plugin handling.
 * 
 */


// Exit if accessed directly.
if ( ! defined( 'WOO_WISH_LIST_DIRECT' ) ) {
	exit; 
}

// Class guard
if ( ! class_exists( 'WooWishList' ) ) :

// Include classes
require_once("class-woo-handler-interface.php");
require_once("class-woo-handler-db.php");
require_once("class-woo-handler-cookie.php");

// TODO:
// Create DB handler class
// Create Cookie handler class

/**
 * Main WooWishList Class.
 *
 * @class WooWishList
 * @version	1.0.0
 */
final class WooWishList {
    
    // This handler is defined by the constructor
    private static $wish_list_handler = null;
    
    /*
     * Initialize this class and by setting the handler.
     * If the user is logged in, use DB handler.
     * Otherwise use the cookie handler.
     */
    public static function init() {
        // If user logged in, use DB
        if ( is_user_logged_in() ) {
            self::$wish_list_handler = new WooWishListDBHandler();
            
            // Update DB from cookie data
            self::update_wishlist_from_cookie();
        } else {
            self::$wish_list_handler = new WooWishListCookieHandler();
        }
        
    }
    
    /*
     * Install plugin by creating a DB table.
     */
    public static function install_plugin() {
        $db_handler = new WooWishListDBHandler();
        $cookie_handler = new WooWishListCookieHandler();
        
        // Try to install the plugin
        if ( ! $db_handler->install() && $cookie_handler->install() ) {
            deactivate_plugins( basename( __FILE__ ) );
            wp_die("Couldn't Install Plugin.");
        }
        
        self::save_to_log();
    }
    
    /*
     * Uninstall plugin by deleting the DB table and all records.
     */
    public static function uninstall_plugin() {
        // Remove wish list table
        $db_handler = new WooWishListDBHandler();
        $cookie_handler = new WooWishListCookieHandler();
        
        $db_handler->uninstall();
        $cookie_handler->uninstall();
        
        self::save_to_log();
    }
    
    /*
     * Setup Frontend assets
     *
     */
    public static function setup_frontend() {
        // Enqueue main css file
        wp_enqueue_style( 'woo-wishlist',
                   plugins_url( '/css/woo-wishlist.css', __DIR__),
                   array('woocommerce-general'),
                   '1.0.0');
        
        // Enqueue main js script. (ajax)
        wp_enqueue_script( 'woo-wishlist',
                           plugins_url( '/js/woo-wishlist.js', __DIR__),
                           array('jquery'),
                           '1.0.0',
                           true);
        
        // Setup ajax inline js parameters
        wp_localize_script( 'woo-wishlist', 'woo_wishlist_params',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    }
    
    /*
     * WooCommerce After "Add To Cart" Hook Handler
     *
     * Adds the "Add to wishlist" button.
     */
    public static function create_add_to_wishlist_button($ID = null) {
        
        if ($ID == null)
        {
            $product_id = get_the_ID();
        }
        else
        {
            $product_id = $ID;
        }
        
        if ( ! empty( $product_id )) {
            
            // Returns the wishlist ID if its found, false otherwise.
            $wishlist_id = self::$wish_list_handler->is_in_wishlist( $product_id );
            
            // Get current plugin folder and concat it with the spinner gif file.
            $spinner_url = plugins_url( 'images/spinner.gif', dirname(__FILE__) );
            
            if ( $wishlist_id === false)
            {
                printf('<div class="woo-wishlist-box">' .
                           '<a data-wwl-product-id="%d" class="woo-wishlist-button woo-wishlist-add">' .
                               '<img class="wwl-spinner" alt="woo wish list spinner" src="%s" />' .
                               '<span class="wwl-button-text">%s</span>' .
                           '</a>' .
                       '</div>',
                       $product_id,
                       $spinner_url,
                       __( "Add to wish list", WOO_WISH_LIST_TEXT_DOMAIN ));
            } else {
                printf('<div class="woo-wishlist-box">' .
                           '<a data-wwl-product-id="%d" data-wwl-wishlist-id="%d" class="woo-wishlist-button woo-wishlist-remove">' .
                               '<img class="wwl-spinner" alt="woo wish list spinner" src="%s" />' .
                               '<span class="wwl-button-text">%s</span>' .
                           '</a>' .
                       '</div>',
                       $product_id,
                       $wishlist_id,
                       $spinner_url,
                       __( "Remove from wish list", WOO_WISH_LIST_TEXT_DOMAIN ));
            }
        }
    }
    
    
    // Add hooks for ajax requests
    
    /*
     *  Add Product to wishlist
     *
     *  @note If the product is already in the wish list, the ID is returned immediately.
     *  @note If user is logged in, the product will be added to the DB, otherwise it will be stored in a cookie.
     *  @note This function assumes that action_type, product_id / wishlist_id are present in the POST params.
     *
     *  @return boolean             On SUCCESS returns newly created wish list ID.
     *                              If one of the parameters were invalid or DB error, returns false
     *
     */
    public static function handle_wishlist_request() {
        $product_id = 0;
        $action_type = "add";
        
        // Try to get product ID from post
        $action_type = filter_input( INPUT_POST, 'action_type', FILTER_SANITIZE_SPECIAL_CHARS );

        if ( ! empty( $action_type ) ) {
            
            // Check the action type. Consider changing to switch if more types are added.
            if ( $action_type == "add" ) {
                $product_id = intval( filter_input( INPUT_POST, 'product_ID', FILTER_SANITIZE_NUMBER_INT ) );
                
                // Product ID exists
                if ( ! empty ( $product_id ) ) {
                    
                    $wishlist_id = self::$wish_list_handler->add_product_to_wishlist( $product_id );
                    
                    if ( $wishlist_id === false ) {
                        self::json_output_error( __( "Couldn't add to wish list.", WOO_WISH_LIST_TEXT_DOMAIN) );
                        
                        // Log
                    } else {
                        
                        // Success
                        self::json_output_success( __( "Added to wish list.", WOO_WISH_LIST_TEXT_DOMAIN),
                                                  array(
                                                      "button_text" => "Remove from wishlist",
                                                      "wishlist_id" => $wishlist_id
                                                  ));
                    }
                }
            } elseif ( $action_type == "remove" ) {
                $wishlist_id = intval( filter_input( INPUT_POST, 'wishlist_ID', FILTER_SANITIZE_NUMBER_INT ) );
                
                if ( ! empty( $wishlist_id ) ) {
                    if ( ! self::$wish_list_handler->remove_product_from_wishlist( $wishlist_id )) {
                        
                        self::json_output_error( __( "Couldn't remove wish list item.", WOO_WISH_LIST_TEXT_DOMAIN) );
                        
                        // Log
                    } else {
                        
                        // Success
                        self::json_output_success( __( "Removed from wish list.", WOO_WISH_LIST_TEXT_DOMAIN), array("button_text" => "Add to wish list"));
                    }
                }
            } else {
                
                // Log
            }
        }
        
        
        // Error out and stop execution.
        self::json_output_error( __( "Couldn't parse wishlist request.", WOO_WISH_LIST_TEXT_DOMAIN ) );
    }
    
    /*
     *  Update DB Wish List From Cookie
     *
     *  @note   If user is registered and wish list cookie isn't empty
     *          it will update the DB with wishlist information.
     * 
     */
    public static function update_wishlist_from_cookie() {
        $db_handler = new WooWishListDBHandler();
        $cookie_handler = new WooWishListCookieHandler();
        
        $cookie_wishlist = $cookie_handler->get_full_wish_list();
        
        if ( ! empty( $cookie_wishlist ) ) {
            foreach ( $cookie_wishlist as $wish ) {
                // This function might fail when trying to insert an already existing id. Its ok.
                $db_handler->add_product_to_wishlist( $wish["product_ID"] );
            }
        }
        
        $cookie_handler->uninstall();
    }
    
    /*
     *  Retrive Wish List By User ID Or Cookie.
     *
     *  @note If user is logged in, it will retrive the list from the DB, Otherwise it will generate the list from the cookie. 
     *
     *  @return Mixed               On SUCCESS returns Object array with all the wish list items for current user (empty if nothing was found).
     *                              If DB error, return false.
     *
     */
    public static function get_full_wish_list() {
        
    }
    
    /*
     *  Retrive Wish List By Product ID.
     *
     *  @note If user is logged in, it will retrive the wish list info from the DB, Otherwise it will retrive the item from the cookie. 
     *
     *  @return Mixed               On SUCCESS returns the item object (empty if nothing was found).
     *                              If DB error, returns false.
     *
     */
    public static function get_single_wish_list() {
        
    }
    
    /*
     * Display Wish list
     *
     * Used with shortcode to display a table of products that were wished for.
     *
     */
    public static function display_wishlist() {
        $wishes = self::$wish_list_handler->get_full_wish_list();
        
        if ( ! empty( $wishes ))
        {
            echo '<table class="woo-wishlist">';

            foreach ( $wishes as $wish )
            {
                $product_id = intval( $wish["product_ID"] );
                $wish_id = intval( $wish["ID"] );
                $product = wc_get_product( $product_id );

                if ( ! empty( $product ))
                {
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'thumbnail' );
                    $post_title = $product->post->post_title;

                    echo '<tr class="woo-wishlist-row">';
                        printf( '<td class="woo-wishlist-image"><img src="%s" alt="%s" title="%s" /></td>', $image[0], $post_title, $post_title);
                        printf( '<td class="woo-wishlist-title"><span>%s</span></td>', $post_title);
                        printf( '<td class="woo-wishlist-price"><span>%s%s</span></td>', $product->get_price(), get_woocommerce_currency_symbol());

                        // Get Add to cart button
                        echo '<td class="woo-wishlist-addtocart">';
                        echo do_shortcode("[add_to_cart id=". $product_id ."]");
                        self::create_add_to_wishlist_button( $product_id );
                        echo '</td>';
                    echo '</tr>';
                }
            }

            echo '</table>';
        }
        else
        {
            _e("Nothing in the wishlist." );
        }
    }
    
    /*
     * Handle Plugin Localization
     */
    public static function localization() {
        load_plugin_textdomain( WOO_WISH_LIST_TEXT_DOMAIN, false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
    }
    
    
        
    /*
     * Output AJAX JSON success result
     *
     * @param $Message      Message to print
     * @param $ExtraData    More information to pass back
     */
    private static function json_output_success( $Message, $ExtraData ) {
        $output_array = array("status" => "success", "message" => $Message , "extra" => $ExtraData);
        
        echo json_encode( $output_array );
        
        die();
    }
    
    /*
     * Output AJAX JSON error result
     *
     * @param $Message      Message to print
     */
    private static function json_output_error( $Message )
    {
        $output_array = array("status" => "error", "message" => $Message );
        
        echo json_encode( $output_array );
        
        die();
    }
    
        
    /*
     * Save to debug log
     */
    private static function save_to_log() {
        file_put_contents( __DIR__ . '/wish_list_log.txt', ob_get_contents() );

        ob_flush();
    }
    
}


// End class guard
endif;