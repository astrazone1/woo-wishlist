<?php
/*
 * File : includes/class-woo-handler-cookie.php
 * Date : 23/11/2016
 * Author : Alex P
 *
 * Woo Wish List Cookie Handler
 * This handler stores the wishlist in a cookie and provides functions to 
 * store and retrive the values
 * 
 */

// Exit if accessed directly.
if ( ! defined( 'WOO_WISH_LIST_DIRECT' ) ) {
	exit; 
}

/**
 * Woo Wish List Cookie Handler
 *
 * In this handler product_ID is the wish list ID
 *
 * @class WooWishListCookieHandler
 * @version	1.0.0
 */
final class WooWishListCookieHandler implements iWooWishListHandler{

    // Cookie name
    private $cookie_name = WOO_WISH_LIST_COOKIE_NAME;
    
    // Wish list array
    private $wishlist_array;
    
    /*
     * Constructor for preparing the cookies
     */
    function __construct() {
        $current_cookie = "";
        $this->wishlist_array = array();
        
        // Get cookie
        if ( ! empty ( $_COOKIE[$this->cookie_name] ) ) {
            $current_cookie = $_COOKIE[$this->cookie_name];
            $this->wishlist_array = explode( ",", $current_cookie);
        }
    }
    
    /*
     * Cookie doesn't have install feature
     */
    public function install() {
        return true;
    }
    
    /*
     * Delete the cookie because it's no longer needed
     */
    public function uninstall() {
        setcookie( $this->cookie_name, null, time() - 3600, '/');
    }


    /*
     *  Add Product to wishlist
     *
     *  @param $Product_ID          Product ID to store in the cookie.
     *
     *  @return mixed               Last inserted ID or FALSE if operation failed. 
     */
    public function add_product_to_wishlist( $Product_ID ) {
        if ( ! $this->is_in_wishlist( $Product_ID ) ) {
            $this->wishlist_array[] = $Product_ID;

            // Store cookie
            setcookie( $this->cookie_name, implode( ",", $this->wishlist_array ), strtotime( WOO_WISH_LIST_COOKIE_LIFE_TIME ), '/');
            
            return $Product_ID;
        }
        
        return false;
    }
    
    /*
     *  Is the product in the wishlist
     *
     *  @param $Product_ID      Product ID to check
     *
     *  @return mixed           ID of wishlist record
     *                          FALSE no record found
     */
    public function is_in_wishlist( $Product_ID ) {
        if ( in_array( $Product_ID, $this->wishlist_array ) ) {
            return $Product_ID;
        }
        
        return false;
    }
    
    /*
     *  Remove Product from wishlist.
     *
     *  @param $Wishlist_ID     ID to remove
     *
     *  @return boolean         TRUE if removed
     *                          FALSE if removal failed
     */
    public function remove_product_from_wishlist( $Wishlist_ID ) {
        // Search value in array and delete it
        if ( ( $key = array_search( $Wishlist_ID , $this->wishlist_array) ) !== false ) {
            
            // Remove found element
            unset( $this->wishlist_array[$key] );
            
            // Store cookie
            setcookie( $this->cookie_name, implode( ",", $this->wishlist_array ), strtotime( WOO_WISH_LIST_COOKIE_LIFE_TIME ), '/');
            
            return true;
        }

        return false;
    }
    
    /*
     *  Retrive Wish List from the cookie.
     *
     *  @note In cookie handler wishlist ID is product ID
     *
     *  @return Array       Each element is array with product_id and wishlist_id
     */
    public function get_full_wish_list() {
        $returnArray = array();
        
        foreach ( $this->wishlist_array as $product_ID ) {
            $wish = array( "product_ID" => intval( $product_ID ), "ID" => intval( $product_ID ) );
            
            $returnArray[] = $wish;
        }
        
        return $returnArray;
    }
    
    /*
     *  Retrive Wish List By Product ID.
     *
     *  @param $Product_ID      Product id to retrive the wishlist ID
     *
     *  @return int             Wishlist ID or false if not found
     */
    public function get_single_wish_list( $Product_ID ) {
        return $this->is_in_wishlist( $Product_ID );
    }
    
}