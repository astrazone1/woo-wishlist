/*
 * File : js/woo-wishlist.js
 * Date : 22/11/2016
 * Author : Alex P
 *
 * Woo Wish List Javascript file
 * Handles the AJAX requests to the plugin and other clienside functions
 * 
 */

jQuery(document).ready(function($) {
    
    var global_parameters = woo_wishlist_params;
    var ajax_action_name = 'woo_wishlist_request';
    var in_progress = false;
    
    function handleWishlistRequest(wish_btn, action, data) {
        var product_id = wish_btn.data("wwl-product-id");
        var wishButton = wish_btn;
        var wishButtonText = wish_btn.find(".wwl-button-text");
        var spinner = wish_btn.find(".wwl-spinner");
        
        // Don't allow multiple requests
        if (in_progress)
        {
            return false;
        }
        
        in_progress = true;
        
        // Set spinner
        spinner.css("display", "block");
        
        // We can also pass the url value separately from ajaxurl for front end AJAX implementations
        $.post(global_parameters.ajax_url, data, function(response) {
            try {
                // Try to parse the data received as json
                var json_data = $.parseJSON(response);

                // Close spinner
                spinner.css("display", "none");
                
                if (json_data.status == "success") {
                    // Added to wishlist
                    wishButtonText.text(json_data.extra.button_text);
                    wishButton.data("wwl-wishlist-id", json_data.extra.wishlist_id);
                    
                    // Switch classes
                    if (action == "add") {
                        wishButton.removeClass("woo-wishlist-add");
                        wishButton.addClass("woo-wishlist-remove");
                    }
                    else
                    {
                        wishButton.removeClass("woo-wishlist-remove");
                        wishButton.addClass("woo-wishlist-add");
                        
                        // Remove data
                        wishButton.data("wwl-wishlist-id", "");
                    }
                }
                else {
                    wishButton.after('<span class="woo-wishlist-error">' + json_data.message + '</span>');
                }
            }
            catch (e) {
                console.log(e);
                // Unknown error. delete button for now.
                wishButton.remove();
            }
            
            in_progress = false;
        });
        
        return false;
    }
    
    // Add to wishlist button
    $("#content").on('click', ".woo-wishlist-add", function(e) {
        e.preventDefault();
        
        var product_id = $(this).data("wwl-product-id");
        
        if (typeof(product_id) != 'undefined') {
            var data = {
                'action': ajax_action_name,
                'action_type': "add",
                'product_ID': product_id
            };
            
            handleWishlistRequest($(this), "add", data);
        }
        
        return false;
    });
    
    // Remove from wishlist button
    $("#content").on('click', ".woo-wishlist-remove", function(e) {
        e.preventDefault();
        
        var wishlist_id = $(this).data("wwl-wishlist-id");
        
        if (typeof(wishlist_id) != 'undefined') {
            var data = {
                'action': ajax_action_name,
                'action_type': "remove",
                'wishlist_ID': wishlist_id
            };
            
            handleWishlistRequest($(this), "remove", data);
        }
        
        
        return false;
        /*var wishlist_id = $(this).data("wwl-wishlist-id");
        var wishButton = $(this);
        var wishButtonText = $(this).find(".wwl-button-text");
        
        
        
        // Don't allow multiple requests
        if (in_progress)
        {
            return false;
        }
        
        in_progress = true;
        
        // Set spinner
        var spinner = $(this).find(".wwl-spinner");

        spinner.css("display", "block");
        
        if (typeof(wishlist_id) != 'undefined') {
            var data = {
                'action': ajax_action_name,
                'action_type': "remove",
                'wishlist_ID': wishlist_id
            };
            
            // We can also pass the url value separately from ajaxurl for front end AJAX implementations
            $.post(global_parameters.ajax_url, data, function(response) {
                try {
                    // Try to parse the data received as json
                    var json_data = $.parseJSON(response);

                    // Remove spinner
                    spinner.css("display", "none");
                    
                    if (json_data.status == "success") {
                        // Added to wishlist
                        wishButtonText.text(json_data.extra.button_text);
                        
                        // Switch class to remove
                        wishButton.removeClass("woo-wishlist-remove");
                        wishButton.addClass("woo-wishlist-add");
                        
                        // Remove data
                        wishButton.data("wwl-wishlist-id", "");
                    }
                    else {
                        wishButton.after('<span class="woo-wishlist-error">' + json_data.message + '</span>');
                    }
                }
                catch (e) {
                    console.log(e);
                    // Unknown error. delete button for now.
                    wishButton.remove();
                }
                
                in_progress = false;
            });
        }
        
        return false;*/
    });
});