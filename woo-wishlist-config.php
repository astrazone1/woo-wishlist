<?php
/*
 * File : woo-wishlist-config.php
 * Date : 21/11/2016
 * Author : Alex P
 *
 * Woo Wishlist configuration file
 * 
 */

// Direct access guard
define( "WOO_WISH_LIST_DIRECT", true );

// DB Table name (table_prefix defined by wp-config.php)
define( "WOO_WISH_LIST_DB_TABLE_NAME", "woo_wishlist" );

// Cookie name
define( "WOO_WISH_LIST_COOKIE_NAME", "woo_wishlist" );

// Cookie Lifetime
define( "WOO_WISH_LIST_COOKIE_LIFE_TIME", "+30 days" );

// Text Domain
define( "WOO_WISH_LIST_TEXT_DOMAIN", "woo_wishlist" );